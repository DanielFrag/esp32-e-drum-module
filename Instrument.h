#ifndef Instrument_h
#define Instrument_h

#include "Arduino.h"

class Instrument {
  public:
    Instrument(const byte* sampleAddress, unsigned long sampleSize);
    void handleSensorData(int sensorId, byte data);
    byte getData();
  private:
    const byte* _sampleAddress;
    unsigned long _sampleSize;
    unsigned long _currentSampleIndex;
};

#endif
