# esp32-e-drum-module

## Synopsis
Proof of concept: e-drum.   
Implements an e-drum using the esp32. The e-drum has 3 pieces: kick, hihat (closed) and snare.

## Parts list

Item | Qty
-----|--------------
Resistors 1M | 3
Piezos | 3
NodeMCU ESP32S | 1
Phone Jack | 1

## Schematic

```
/*
 *             GND      GND      GND   
 *             _|__     _|__     _|__  
 *            |    |   |    |   |    | 
 *     OUT    P    R   P    R   P    R 
 *      |     |____|   |____|   |____| 
 *    __|_______|________|________|____
 *   | 25      32       34       35    |
 *   |                                 |
 *   |             ESP32               |
 * 
 * P = Piezos
 * R = Resistors 1M
 * OUT = Phone Jack
 * GND = esp32 gnd
*/
```
## Instructions
Rename the file `PCMs_example.h` to `PCMs.h` and, using the Arduino IDE, load the code on esp32 module.

## How it works
This code is based on two tasks, that runs on separate cores, and a timer interrupt routine (TIR). The TIR is used to define the audio sample rate: 32 kHz. Only through the TIR the global flag `writeSample` is setted to `true`.   
The first task runs on core 0. It is used to update the sensors state. When an event occurs, on the sensor, the event info (recipient and data) is writed on an events queue.    
The second task, the loop function, runs on core 1. First, it read the events from the events queue and send each event to the recipient instrument. Second, it checks if is necessrary to write the audio data on the output (`writeSample` equals to `true`). If so, it calculates the states of the instruments, sends the data to the internal DAC and sets the `writeSample` flag to `false`.
```
  | Core 0 |                 | Core 1 |
  |        |                 |        |                       |        |
  | Task 1 | ---> Queue ---> | Task 2 | ---> writeSample <--- |  TIR   |
  |        |                 |        |                       |        |
```
