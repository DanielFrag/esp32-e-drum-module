#include "Instrument.h"

Instrument::Instrument (const byte* sampleAddress, unsigned long sampleSize) {
  _sampleAddress = sampleAddress;
  _sampleSize = sampleSize;
  _currentSampleIndex = 0;
}

void Instrument::handleSensorData(int sensorId, byte data) {
  _currentSampleIndex = 0;
}

byte Instrument::getData() {
  if (_currentSampleIndex < _sampleSize) {
    return _sampleAddress[_currentSampleIndex++];
  }
  return B10000000;
}
