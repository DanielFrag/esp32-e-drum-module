#ifndef EventsBroker_h
#define EventsBroker_h

#include "Arduino.h"
#include "Instrument.h"

class EventsBroker {
  public:
    EventsBroker(QueueHandle_t eventsQueue, int numOfAddress);
    bool setReceiver(int address, Instrument* instrument);
    void dequeueEvent();
  private:
    QueueHandle_t _eventsQueue;
    int _numOfAddress;
    Instrument** _addresses;
    void _handleEvent(byte eventData);
};

#endif
