/*
 * Proof of concept: e-drum
 * Implements an e-drum using the esp32. The e-drum has 3 pieces: kick, hihat (closed) and snare.
 * 
 * Parts list
 * Resistors 1M (x3)
 * Piezos (x3)
 * NodeMCU ESP32S (x1)
 * 
 * Wires
 *             GND      GND      GND   
 *             _|__     _|__     _|__  
 *            |    |   |    |   |    | 
 *     OUT    P    R   P    R   P    R 
 *      |     |____|   |____|   |____| 
 *    __|_______|________|________|____
 *   | 25      32       34       35    |
 *   |                                 |
 *   |             ESP32               |
 * 
 * P = Piezos
 * R = Resistors 1M
 * OUT = Phone Jack
 * GND = esp32 gnd
*/

#define QUEUE_SENSOR_EVENTS_SIZE 12
#define KICK_PIN 34
#define SNARE_PIN 35
#define HIHAT_PIN 32

#include <WiFi.h>
#include "PCMs.h"
#include "Sensor.h"
#include "Instrument.h"
#include "EventsBroker.h"
#include "Mixer.h"

QueueHandle_t xQueueSensorEvents;
TaskHandle_t sensorHandlerTask;
hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

// tick to audio updates
volatile bool writeSample = false;

void handleSensorList();
void handleInstrumentList();
void sensorHandler(void* parameter);
void configInterrupt();
void IRAM_ATTR onTimer();

const size_t kickArraySize = sizeof(kickPcm);
const size_t snareArraySize = sizeof(snarePcm);
const size_t closedHiHayArraySize = sizeof(closedHiHatPcm);
Instrument* kickInst;
Instrument* snareInst;
Instrument* hihatInst;
Sensor* kickSensor;
Sensor* snareSensor;
Sensor* hihatSensor;
Mixer* mixer;
EventsBroker* eventsBroker;

void setup () {
  // unused resources.
  WiFi.mode(WIFI_OFF);
  btStop();
  // "singleton" items.
  xQueueSensorEvents = xQueueCreate(QUEUE_SENSOR_EVENTS_SIZE, sizeof(byte));
  eventsBroker = new EventsBroker(xQueueSensorEvents, 3);
  mixer = new Mixer(3);
  // create sensors and instruments instances and link it with the "singleton" items.
  handleSensorList();
  handleInstrumentList();
  // Task to update sensors state. This process is to slow, therefore, it was separated from the audio processing.
  xTaskCreatePinnedToCore(
    sensorHandler,          /* Task function. */
    "sensorHandler",        /* name of task. */
    10000,                  /* Stack size of task */
    NULL,                   /* parameter of the task */
    1,                      /* priority of the task */
    &sensorHandlerTask,     /* Task handle to keep track of created task */
    0                       /* pin task to core 0 */
  );
  // Set the "clock" for audio updates
  configInterrupt();
}

void loop () {
  bool shouldWrite = false;
  (*eventsBroker).dequeueEvent();
  portENTER_CRITICAL(&timerMux);
  if (writeSample) {
    shouldWrite = writeSample;
    writeSample = false;
  }
  portEXIT_CRITICAL(&timerMux);
  if (shouldWrite) {
    (*mixer).writeAudioData();
  }
}

void handleSensorList() {
  kickSensor = new Sensor(0, xQueueSensorEvents, KICK_PIN, 50, 100, 1000);
  snareSensor = new Sensor(1, xQueueSensorEvents, SNARE_PIN, 50, 100, 1000);
  hihatSensor = new Sensor(2, xQueueSensorEvents, HIHAT_PIN, 50, 100, 1000);
}

void handleInstrumentList() {
  kickInst = new Instrument(kickPcm, kickArraySize);
  (*eventsBroker).setReceiver(0, kickInst);
  (*mixer).setChannel(0, kickInst);

  snareInst = new Instrument(snarePcm, snareArraySize);
  (*eventsBroker).setReceiver(1, snareInst);
  (*mixer).setChannel(1, snareInst);

  hihatInst = new Instrument(closedHiHatPcm, closedHiHayArraySize);
  (*eventsBroker).setReceiver(2, hihatInst);
  (*mixer).setChannel(2, hihatInst);
}

void sensorHandler(void* parameter) {
  while (true) {
    (*kickSensor).updateSensor();
    (*snareSensor).updateSensor();
    (*hihatSensor).updateSensor();
    delay(1);
  }
}

void configInterrupt() {
  timer = timerBegin(0, 500, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, 5, true);
  timerAlarmEnable(timer);
}

void IRAM_ATTR onTimer() {
  portENTER_CRITICAL_ISR(&timerMux);
  writeSample = true;
  portEXIT_CRITICAL_ISR(&timerMux);
}
