#include "EventsBroker.h"

EventsBroker::EventsBroker(QueueHandle_t eventsQueue, int numOfAddress) {
  _eventsQueue = eventsQueue;
  _numOfAddress = numOfAddress;
  _addresses = (Instrument**) malloc(_numOfAddress * sizeof(Instrument*));
  for (int i = 0; i < _numOfAddress; i++) {
    _addresses[i] = nullptr;
  }
}

bool EventsBroker::setReceiver(int address, Instrument* instrument) {
  if (address < 0 || address > _numOfAddress || _addresses[address] != nullptr) {
    return false;
  }
  _addresses[address] = instrument;
  return true;
}

void EventsBroker::dequeueEvent() {
  byte queueData = B00000000;
  if (xQueueReceive(_eventsQueue, &(queueData), 0) == pdPASS) {
    _handleEvent(queueData);
  }
}

void EventsBroker::_handleEvent(byte eventData) {
  int address = (eventData >> 4);
  byte data = (eventData & B00001111);
  (*_addresses[address]).handleSensorData(address, data);
}