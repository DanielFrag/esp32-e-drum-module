#include "Sensor.h"

Sensor::Sensor (int address, QueueHandle_t xQueueSensorEvents, int analogPin, int maxSamples, int backOff, int threshould) {
  _address = address;
  _analogPin = analogPin;
  _maxSamples = maxSamples;
  _backOff = backOff;
  _xQueueSensorEvents = xQueueSensorEvents;
  _threshould = threshould;
  _resetState();
}

void Sensor::updateSensor () {
  int value = analogRead(_analogPin);
  switch (_state) {
    case 0:
      _handleIdleState(value);
      break;
    case 1:
      _handleTriggeredState(value);
      break;
    case 2:
      _handleBackOffState(value);
      break;
    default:
      _resetState();
      break;
  }
}

void Sensor::_handleIdleState (int sensorVal) {
  if (sensorVal > _threshould) {
    _counterSamples = 0;
    _maxValue = sensorVal;
    _backOffReference = sensorVal;
    _state = 1;
  }
}

void Sensor::_handleTriggeredState (int sensorVal) {
  _counterSamples++;
  if (sensorVal > _maxValue) {
    _backOffReference = _maxValue;
    _maxValue = sensorVal;
  }
  if (_counterSamples == _maxSamples) {
    byte data = (_address << 4) | B00000001;
    xQueueSend(_xQueueSensorEvents, (void *) &data, portMAX_DELAY);
    _counterSamples = 0;
    _state = 2;
  }
}

void Sensor::_handleBackOffState (int sensorVal) {
  if (sensorVal > _backOffReference) {
    _maxValue = sensorVal;
    _counterSamples = 0;
    _state = 1;
    return;
  }
  if (sensorVal < _threshould) {
    _counterSamples++;
  } else {
    _counterSamples = 0;
  }
  if (_counterSamples == _backOff) {
    _resetState();
  }
}

void Sensor::_resetState () {
  _state = 0;
  _maxValue = 0;
  _backOffReference = 0;
  _counterSamples = 0;
}
