#ifndef Sensor_h
#define Sensor_h

#include "Arduino.h"

class Sensor {
  public:
    Sensor(int address, QueueHandle_t xQueueSensorEvents, int analogPin, int maxSamples, int backOff, int threshould);
    void updateSensor();
  private:
    int _address;
    int _counterSamples;
    int _analogPin;
    int _maxSamples;
    int _backOff;
    QueueHandle_t _xQueueSensorEvents;
    int _threshould;
    int _maxValue;
    int _backOffReference;
    int _state; // 0 = idle, 1 = triggered, 2 = backOff
    void _handleIdleState(int sensorVal);
    void _handleTriggeredState(int sensorVal);
    void _handleBackOffState(int sensorVal);
    void _resetState();
};

#endif
