#ifndef Mixer_h
#define Mixer_h

#include "Arduino.h"
#include "Instrument.h"

class Mixer {
  public:
    Mixer(int numOfChannels);
    void writeAudioData();
    bool setChannel(int channelIndex, Instrument* instrument);
  private:
    int _numOfChannels;
    Instrument** _channels;
    bool _idleFlag;
    static const int DAC_PIN = 25;
};

#endif
