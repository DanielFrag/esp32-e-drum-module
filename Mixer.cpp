#include "Mixer.h"

Mixer::Mixer(int numOfChannels) {
  _numOfChannels = numOfChannels;
  _idleFlag = true;
  _channels = (Instrument**) malloc(_numOfChannels * sizeof(Instrument*));
  for (int i = 0; i < _numOfChannels; i++) {
    _channels[i] = nullptr;
  }
}

void Mixer::writeAudioData() {
  int acc = 0;
  for (int i = 0; i < _numOfChannels; i++) {
    if (_channels[i] != nullptr) {
      acc += ((int) (*_channels[i]).getData() - 128);
    }
  }
  if (!acc) {
    if (!_idleFlag) {
      _idleFlag = true;
      dacWrite(Mixer::DAC_PIN, 128);
    }
    return;
  }
  if (_idleFlag) {
    _idleFlag = false;
  }
  acc += 128;
  acc = constrain(acc, 0, 255);
  dacWrite(Mixer::DAC_PIN, (byte) acc);
}

bool Mixer::setChannel(int channelIndex, Instrument* instrument) {
  if (channelIndex < 0 || channelIndex > _numOfChannels || _channels[channelIndex] != nullptr) {
    return false;
  }
  _channels[channelIndex] = instrument;
  return true;
}
